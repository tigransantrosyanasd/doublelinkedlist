#include <iostream>
#include <exception>
#include "DoubleLInkedList.h"


class myException : public std::exception {
public:
    myException(const char* utterance1):utterance(utterance1){}
    const char* what()const noexcept {return utterance;}

private:
    const char* utterance;
};

template <typename  T>
DoubleLinkedList<T>::DoubleLinkedList()
        :m_head(nullptr)
        ,m_size(0){}

template <typename T>
DoubleLinkedList<T>::~DoubleLinkedList() {
    Node<T>* current;
    while(m_head->next != nullptr) {
        current = m_head->m_next;
        delete m_head;
        m_head = current;
    }
    delete m_head;
}

template <typename T>
void DoubleLinkedList<T>::push(T info) {
    if(m_size == 0) {
        m_head = new Node<T>();
        m_head->m_info = info;
    }
    else {
        Node<T>* current = m_head;
        while(current->m_next != nullptr ) {
            current = current->m_next;
        }
        Node<T>* new_element = new Node();
        new_element->m_previous = current;
        new_element->m_next = nullptr;
        new_element->m_info = info;
    }
    ++m_size;
}

template <typename T>
void DoubleLinkedList<T>::insert(int pos, T info){
    if(pos <= 0 || pos > m_size) {
        throw myException("Incorrect position");
    }
    if(pos == m_size || m_size == 0) {
        push(info);
    }
    else {
        Node<T>* current = m_head;
        for (int i = 1; i < pos; i++) {
            current = current->m_next;
        }
        Node<T>* new_element = new Node();
        new_element->m_next = current->m_next;
        new_element->m_info = info;
        new_element->m_previous = current;
        current->m_next = new_element;
        new_element->m_next->m_previous = new_element;
        m_size++;
    }
}

template <typename T>
void DoubleLinkedList<T>::remove(int pos){
    if(m_size == 0) {
        throw myException("List is empty");
    }
    if(pos <= 0 || pos > m_size) {
        throw myException("Incorrect position");
    }
    Node<T>* current = m_head;
    if(pos == 1) {
        current->m_next->m_previous = nullptr;
        m_head = m_head->m_next;
    }
    else {
        for (int i = 1; i < pos; i++) {
            current = current->m_next;
        }
        if(pos == m_size) {
            current->m_previous->m_next = nullptr;
        }
        else {
            current->m_previous->m_next = current->m_next;
            current->m_next->m_previous = current->m_previous;
        }
        delete current;
        m_size--;
    }
}