#pragma once
template <typename  T>
class DoubleLinkedList {
public:
    DoubleLinkedList();
//    DoubleLinkedList(const DoubleLinkedList<T>& a) = delete;
//    DoubleLinkedList<T>& operator =(const DoubleLinkedList& a) = delete;
    ~DoubleLinkedList();
    void push(T info);
    void insert(int pos, T info);
    void remove(int pos);
    T& head() { return m_head->m_info; }
    bool is_empty() { return m_head == nullptr; }
    int size()const { return m_size; }
    void print() {
        Node<T>* ptr = m_head;
        while (ptr != nullptr) {
            std::cout << ptr->m_info << " , ";
            ptr = ptr->m_next;
        }
    }
private:
    template<typename T>
    struct Node {
        Node():m_next(nullptr), m_previous(nullptr){}
        Node(T info):m_info(info),m_next(nullptr),m_previous(nullptr){}
        T m_info;
        Node<T>* m_next;
        Node<T>* m_previous;
    };
    Node<T>* m_head;
    int m_size;
};